Gem::Specification.new do |spec|
  spec.name = 'etl'
  spec.version = '0.0.0'
  spec.summary = 'ETL library'
  spec.description = ' '

  spec.authors = ['Joseph Choe']
  spec.email = ['joseph@josephchoe.com']
  spec.homepage = 'https://github.com/josephchoe/mediavine-interview'

  spec.require_paths = ['lib']
  spec.platform = Gem::Platform::RUBY
  spec.required_ruby_version = '>= 2.6'

  files = Dir['lib/**/*.rb']
  files += Dir['data/**/*']

  spec.files = files

  spec.add_runtime_dependency 'ntl-actor'

  spec.add_runtime_dependency 'evt-log'
  spec.add_runtime_dependency 'evt-telemetry'
end
