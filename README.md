# survey-code-challenge

This repository is an answer to this challenge:

[Survey Code Challenge](https://gist.github.com/Cosrnos/5142075b4a75cacf8f260f181a82fdef)

## Installation

To install, run:

```bash
make
```

Or install development dependencies:

```bash
POSTURE=development make
```

## Usage

To execute the script, run the following in your terminal:

```bash
ruby ./script/start.rb
```
