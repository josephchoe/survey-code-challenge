module ETL
  module Consumer
    module SourceMacro
      def source_macro(source_class)
        @source_class = source_class
      end
      alias source source_macro

      def source_class
        @source_class
      end
    end
  end
end
