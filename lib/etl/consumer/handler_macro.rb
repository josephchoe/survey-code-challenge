module ETL
  module Consumer
    module HandlerMacro
      def handler_macro(handler=nil, &block)
        handler ||= block

        handler_registry.register(handler)
      end
      alias_method :handler, :handler_macro

      def handler_registry
        @handler_registry ||= HandlerRegistry.new
      end
    end
  end
end
