module ETL
  module Consumer
    Tuple = Struct.new(:data) do
      def [](key)
        self.data[key]
      end
    end
  end
end
