module ETL
  module Consumer
    class HandlerRegistry
      include Log::Dependency

      def self.configure(receiver, attr_name: nil, **kwargs)
        attr_name ||= :handler_registry
        instance = build(**kwargs)
        receiver.public_send("#{attr_name}=", instance)
      end

      def entries
        @entries ||= Set.new
      end

      def count
        entries.count
      end

      def each(&block)
        entries.each(&block)
      end

      def each_address(&block)
        each do |handler, handler_address, reply_address|
          block.call(handler_address, reply_address)
        end
      end

      def register(handler)
        logger.trace { "Registering handler (Handler: #{handler.name})" }

        reply_address = Actor::Messaging::Address.build
        handler_address = handler.start(reply_address)

        entries << [handler, handler_address, reply_address]

        logger.debug { "Handler registered (Handler: #{handler.name})" }

        handler
      end
    end
  end
end
