module ETL
  module Consumer
    class Log < ::Log
      def tag!(tags)
        tags << :consumer
      end
    end
  end
end
