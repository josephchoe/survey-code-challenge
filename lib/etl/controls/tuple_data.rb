module ETL
  module Controls
    module TupleData
      def self.example
        data = {
          :test => 'test'
        }
        ETL::Consumer::Tuple.new(data)
      end
    end
  end
end
