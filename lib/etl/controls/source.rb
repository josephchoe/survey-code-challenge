module ETL
  module Controls
    module Source
      def self.example
        instance = ::ETL::Source::CSV::Substitute.build(tuples)
        instance
      end

      def self.tuples
        [
          {
            :column1 => "test1",
            :column2 => "test2"
          }
        ]
      end

      module Valid
        def self.example
          ::ETL::Source::CSV.build(source)
        end

        def self.source
          "#{Dir.pwd}/lib/etl/controls/data/test.csv"
        end
      end

      module Invalid
        def self.example
          ::ETL::Source::CSV.build(invalid_source)
        end

        def self.invalid_source
          "#{Dir.pwd}/lib/etl/controls/data/invalid_utf.csv"
        end
      end
    end
  end
end
