module ETL
  module Controls
    module Consumer
      def self.example
        cls = example_class

        cls.new
      end

      def self.example_class(handlers: nil)
        if handlers == :none
          handlers = []
        else
          handlers ||= [
            Handle.example_class,
            Handle.example_class
          ]
        end

        Class.new do
          include ::ETL::Consumer

          def initialize; end

          handlers.each do |handler_cls|
            handler handler_cls
          end

          def handled_messages
            handled_messages = []

            self.class.handler_registry.each do |handler, handler_address, reply_address|
              next unless handler.ancestors.include?(Handler)

              handled_messages += handler.handled_messages
            end

            handled_messages
          end
        end
      end

      Example = self.example_class
    end
  end
end
