module ETL
  module Controls
    module Handlers
      module AverageSalary
        def self.example
          address = Address.example

          ::Handlers::AverageSalary.new(address)
        end

        module Address
          def self.example
            ::Actor::Messaging::Address.build
          end
        end

        module Tuple
          def self.example
            salary = Salary.example

            TupleData.new(salary)
          end

          module Salary
            def self.example
              100
            end
          end
        end

        module Started
          def self.example
            handler = AverageSalary.example

            handler.handle_start

            handler.total_salary = TotalSalary.example
            handler.total_persons = TotalPersons.example

            handler
          end

          module TotalSalary
            def self.example
              100
            end
          end

          module TotalPersons
            def self.example
              1
            end
          end
        end

        TupleData = Struct.new(:salary)
      end
    end
  end
end
