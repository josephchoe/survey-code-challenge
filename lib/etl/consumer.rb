module ETL
  module Consumer
    def self.included(cls)
      cls.class_exec do
        include Dependency
        include Log::Dependency

        extend Build
        extend Call

        extend SourceMacro
        extend HandlerMacro

        attr_reader :source_data

        dependency :read

        def initialize(source_data)
          @source_data = source_data
        end
      end
    end

    def call
      read.() do |tuple|
        dispatch(tuple)
      end

      get_results
    end

    def dispatch(tuple)
      self.class.handler_registry.each_address do |handler_address, _|
        Actor::Messaging::Send.(Tuple.new(tuple), handler_address, wait: true)
      end
    end

    def get_results
      index = 0

      self.class.handler_registry.each_address do |handler_address, reply_address|
        Actor::Messaging::Send.(:result, handler_address, wait: true)

        result = Actor::Messaging::Read.(reply_address)

        index += 1

        print "#{index}. #{result}\n"
      end
    end

    module Build
      def build(source_data)
        instance = new(source_data)
        self.source_class.configure(instance, source_data)
        instance
      end
    end

    module Call
      def call(*arguments)
        instance = build(*arguments)
        instance.call
      end
    end
  end
end
