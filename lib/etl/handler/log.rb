module ETL
  module Handler
    class Log < ::Log
      def tag!(tags)
        tags << :consumer
      end
    end
  end
end
