require 'etl/controls/source'

require 'etl/controls/handle'
require 'etl/controls/consumer'
require 'etl/controls/tuple_data'

require 'etl/controls/handlers/average_salary'
