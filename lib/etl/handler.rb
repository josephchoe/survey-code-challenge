module ETL
  module Handler
    def self.included(cls)
      cls.class_exec do
        include ::Actor
        include Dependency
        include Log::Dependency

        attr_reader :reply_address

        def initialize(reply_address)
          @reply_address = reply_address
        end
      end
    end
  end
end
