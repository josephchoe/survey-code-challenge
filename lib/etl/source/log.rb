module ETL
  module Source
    class Log < ::Log
      def tag!(tags)
        tags << :source
      end
    end
  end
end
