module ETL
  module Source
    class CSV
      module Substitute
        def self.build(tuples=nil)
          tuples ||= []

          instance = CSV.build
          instance.tuples = tuples

          sink = CSV.register_telemetry_sink(instance)
          instance.sink = sink

          instance
        end

        class CSV < ::ETL::Source::CSV
          attr_accessor :sink,
                        :tuples

          def initialize; end

          def self.build
            instance = new
            instance.configure
            instance
          end

          def reader
            tuples
          end
        end
      end
    end
  end
end
