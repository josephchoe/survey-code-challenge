module ETL
  module Source
    class CSV
      module Telemetry
        class Sink
          include ::Telemetry::Sink

          record :tuple_logged
        end

        Data = Struct.new(:index, :tuple, :message)

        def self.sink
          Sink.new
        end
      end

      def self.register_telemetry_sink(receiver)
        sink = Telemetry.sink
        receiver.telemetry.register(sink)
        sink
      end
    end
  end
end
