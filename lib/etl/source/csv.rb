module ETL
  module Source
    class CSV
      include Dependency
      include Log::Dependency

      attr_reader :source

      dependency :telemetry, ::Telemetry

      def initialize(source)
        @source = source
      end

      def self.build(source)
        instance = new(source)
        instance.configure
        instance
      end

      def self.configure(receiver, source, attr_name: nil)
        attr_name ||= :read
        instance = build(source)
        receiver.public_send("#{attr_name}=", instance)
      end

      def configure
        ::Telemetry.configure(self)
      end

      def call(&blk)
        index = 0

        reader = self.reader

        logger.trace(tag: :csv) { "Reading CSV file..." }

        reader.each do |tuple|
          hash_tuple = tuple.to_hash
          message = nil

          logger.debug(tag: :csv) { "Reading tuple... (Index: #{index}, Tuple: #{hash_tuple})" }

          if block_given?
            message = blk.call(tuple.to_hash)
          end

          telemetry.record :tuple_logged, Telemetry::Data.new(index, hash_tuple, message)

          logger.debug(tag: :csv) { "Read tuple. (Index: #{index}, Tuple: #{hash_tuple})" }

          index += 1
        end

        lines = index + 1

        logger.info(tag: :csv) { "Read CSV file. (Number of Lines: #{lines})" }
      end

      def reader
        csv_file = File.open(source)
        csv_string = csv_file.read.encode!('UTF-8', 'iso-8859-1', invalid: :replace)

        flags = {
          :headers => true,
          :header_converters => :symbol
        }

        reader = ::CSV.new(csv_string, **flags)

        reader
      end
    end
  end
end
