module Handlers
  class MedianAgeMarried
    include ETL::Handler

    HASH_BOOLEAN = {
      "true" => true,
      "false" => false
    }

    attr_accessor :array_age

    handle :start do
      self.array_age = Array.new
    end

    handle :tuple do |tuple|
      married = tuple[:married]

      if !HASH_BOOLEAN[married]
        return
      end

      age = tuple[:age].to_i

      array_age << age
    end

    handle :result do
      sorted_array = array_age.sort
      length = sorted_array.length

      if length % 2 != 0
        median = sorted_array[length/2]
      else
        median = (sorted_array[length/2] + sorted_array[(length/2) - 1]) / 2.0
      end

      send.(median, reply_address)
    end
  end
end
