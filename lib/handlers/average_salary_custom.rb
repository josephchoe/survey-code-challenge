module Handlers
  class AverageSalaryCustom
    include ETL::Handler

    HASH_BOOLEAN = {
      "true" => true,
      "false" => false
    }

    attr_accessor :total_salary,
                  :total_persons

    handle :start do
      self.total_salary = 0
      self.total_persons = 0
    end

    handle :tuple do |tuple|
      salary = tuple[:salary].to_i
      name = tuple[:name]
      married = tuple[:married]
      age = tuple[:age].to_i

      names = name.split(' ')
      last_name = names.last

      if !(last_name =~ /\A[aeiou]/i)
        return
      end

      if HASH_BOOLEAN[married]
        return
      end

      if age < 25 || age > 35
        return
      end

      self.total_salary += salary
      self.total_persons += 1
    end

    handle :result do
      average_salary = total_salary / total_persons
      result = format_result(average_salary)

      send.(result, reply_address)
    end

    def format_result(number)
      result = number.to_s.reverse.gsub(/(\d{3})(?=\d)/, '\\1,').reverse
      "$#{result}"
    end
  end
end
