require 'bigdecimal'
require 'bigdecimal/util'

module Handlers
  class AverageGPALastNameBeginsWithA
    include ETL::Handler

    attr_accessor :total_gpa,
                  :total_persons

    handle :start do
      self.total_gpa = 0.0
      self.total_persons = 0
    end

    handle :tuple do |tuple|
      name = tuple[:name]
      names = name.split(' ')
      last_name = names.last

      if !last_name.start_with?('A')
        return
      end

      gpa = tuple[:gpa].to_d

      self.total_gpa += gpa
      self.total_persons += 1
    end

    handle :result do
      average_gpa = total_gpa / total_persons
      result = format_result(average_gpa)

      send.(result, reply_address)
    end

    def format_result(number)
      "%.1f" % number.truncate(1)
    end
  end
end
