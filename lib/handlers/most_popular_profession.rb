module Handlers
  class MostPopularProfession
    include ETL::Handler

    attr_accessor :hash_jobs

    handle :start do
      self.hash_jobs = Hash.new(0)
    end

    handle :tuple do |tuple|
      job = tuple[:job]

      hash_jobs[job] += 1
    end

    handle :result do
      most_popular_profession, count = hash_jobs.max_by{ |key,value| value }

      send.(most_popular_profession, reply_address)
    end
  end
end
