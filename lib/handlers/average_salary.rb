module Handlers
  class AverageSalary
    include ETL::Handler

    attr_accessor :total_salary,
                  :total_persons

    handle :start do
      self.total_salary = 0
      self.total_persons = 0
    end

    handle :tuple do |tuple|
      salary = tuple[:salary].to_i

      self.total_salary += salary
      self.total_persons += 1
    end

    handle :result do
      average_salary = total_salary / total_persons
      result = format_result(average_salary)

      send.(result, reply_address)
    end

    def format_result(number)
      result = number.to_s.reverse.gsub(/(\d{3})(?=\d)/, '\\1,').reverse
      "$#{result}"
    end
  end
end
