require 'actor'
require 'log'
require 'telemetry'

require 'csv'

require 'etl/source/log'
require 'etl/source/csv/telemetry'
require 'etl/source/csv/substitute'
require 'etl/source/csv'

require 'etl/handler/log'
require 'etl/handler'

require 'etl/consumer/log'
require 'etl/consumer/tuple'
require 'etl/consumer/source_macro'
require 'etl/consumer/handler_registry'
require 'etl/consumer/handler_macro'
require 'etl/consumer'

require 'handlers/average_salary'
require 'handlers/average_gpa_last_name_begins_with_a'
require 'handlers/most_popular_profession'
require 'handlers/median_age_married'
require 'handlers/average_salary_custom'
