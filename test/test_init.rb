ENV['CONSOLE_DEVICE'] ||= 'stdout'
ENV['LOG_LEVEL'] ||= '_min'
ENV['RACK_ENV'] = 'test'

puts RUBY_DESCRIPTION

require_relative '../system/init'

require 'byebug'
require 'test_bench'; TestBench.activate
require 'pp'

require 'etl/controls'

module ETL; end
include ETL
