require_relative '../../automated_init'

context "Handlers" do
  context "Average Salary" do
    context "Tuple" do
      handler = Controls::Handlers::AverageSalary::Started.example
      tuple = Controls::Handlers::AverageSalary::Tuple.example

      prior_total_salary = handler.total_salary
      prior_total_persons = handler.total_persons

      handler.handle_tuple(tuple)

      test "Total Salary is increased by tuple's total salary" do
        assert(handler.total_salary == prior_total_salary + tuple.salary)
      end

      test "Total Persons is increased by 1" do
        assert(handler.total_persons == prior_total_persons + 1)
      end
    end
  end
end
