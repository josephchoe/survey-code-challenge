require_relative '../../automated_init'

context "Handlers" do
  context "Average Salary" do
    context "Result" do
      handler = Controls::Handlers::AverageSalary::Started.example
      reply_address = handler.reply_address

      result = handler.handle_result.first.message

      test "Formatted" do
        assert(result == "$100")
      end
    end
  end
end
