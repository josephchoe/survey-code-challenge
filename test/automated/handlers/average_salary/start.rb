require_relative '../../automated_init'

context "Handlers" do
  context "Average Salary" do
    context "Start" do
      handler = Controls::Handlers::AverageSalary.example

      assert(handler.total_salary.nil?)
      assert(handler.total_persons.nil?)

      handler.handle_start

      test "total_salary" do
        assert(handler.total_salary == 0)
      end

      test "total_persons" do
        assert(handler.total_persons == 0)
      end
    end
  end
end
