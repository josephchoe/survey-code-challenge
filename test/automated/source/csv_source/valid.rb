require_relative '../../automated_init'

context "Source" do
  context "CSV Source" do
    context "Valid" do
      csv_source = Controls::Source::Valid.example

      sink = ETL::Source::CSV.register_telemetry_sink(csv_source)

      expected_result = {
        :column1 => "test1",
        :column2 => "test2"
      }

      csv_source.()

      telemetry_data = sink.tuple_logged_records.first.data

      result = telemetry_data.tuple

      test "Parses result" do
        assert(result == expected_result)
      end
    end
  end
end
