require_relative '../../automated_init'

context "Source" do
  context "CSV Source" do
    context "Invalid" do
      csv_source = Controls::Source::Invalid.example

      sink = ETL::Source::CSV.register_telemetry_sink(csv_source)

      expected_result = {
        :column1 => "\u0093test1\u0094",
        :column2 => "test2"
      }

      csv_source.()

      telemetry_data = sink.tuple_logged_records.first.data

      result = telemetry_data.tuple

      test "Invalid UTF is parsed" do
        assert(result == expected_result)
      end
    end
  end
end
