require_relative '../../automated_init'

context "Source" do
  context "CSV Source" do
    context "Block" do
      csv_source = Controls::Source.example

      sink = ETL::Source::CSV.register_telemetry_sink(csv_source)

      csv_source.() { |x| "test message" }

      logged_message = sink.tuple_logged_records.first.data.message

      test "Block is converted to text" do
        assert(logged_message == "test message")
      end
    end
  end
end
