require_relative '../../automated_init'

context "Source" do
  context "CSV Source" do
    context "Telemetry" do
      csv_source = Controls::Source.example

      sink = ETL::Source::CSV.register_telemetry_sink(csv_source)

      expected_tuple = {
        :column1 => "test1",
        :column2 => "test2"
      }

      csv_source.()

      telemetry_data = sink.tuple_logged_records.first.data

      test 'Telemetry is recorded' do
        recorded = sink.recorded_once? do |record|
          record.signal == :tuple_logged
        end

        assert(recorded)
      end

      test 'index' do
        assert(telemetry_data.index == 0)
      end

      test 'tuple' do
        assert(telemetry_data.tuple == expected_tuple)
      end
    end
  end
end
