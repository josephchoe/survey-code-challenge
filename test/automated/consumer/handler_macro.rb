require_relative '../automated_init'

context "Consumer" do
  context "Handler Macro" do
    consumer = Controls::Consumer.example

    context "Message is dispatched" do
      tuple = Controls::TupleData.example

      consumer.dispatch(tuple)

      test "Message is dispatched to each handler" do
        handled_messages = consumer.handled_messages

        assert(handled_messages.count > 1)
      end
    end
  end
end
