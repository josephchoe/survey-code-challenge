require_relative '../../automated_init'

context "Consumer" do
  context "Handler Registry" do
    context "Each Address" do
      handler = Controls::Handle.example_class
      other_handler = Controls::Handle.example_class

      registry = Consumer::HandlerRegistry.new

      registry.register(handler)
      registry.register(other_handler)

      iterated_handlers = []

      registry.each_address do |handler|
        iterated_handlers << handler
      end

      test "Iterates over each handler" do
        assert(iterated_handlers.count == 2)
      end
    end
  end
end
