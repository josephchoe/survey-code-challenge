require_relative '../../automated_init'

context "Consumer" do
  context "Handler Registry" do
    context "Register" do
      handler = Controls::Handle::Example

      registry = Consumer::HandlerRegistry.new

      registry.register(handler)

      test "Length is increased" do
        assert(registry.count == 1)
      end
    end
  end
end
