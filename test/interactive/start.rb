ENV['LOG_LEVEL'] ||= 'trace'
ENV['LOG_TAGS'] ||= '_all'

require_relative '../../system/init'

survey_data = "#{Dir.pwd}/data/survey_data1.csv"

class Consumer
  include ETL::Consumer

  source ETL::Source::CSV

  handler Handlers::AverageSalary
  handler Handlers::AverageGPALastNameBeginsWithA
  handler Handlers::MostPopularProfession
  handler Handlers::MedianAgeMarried
  handler Handlers::AverageSalaryCustom
end

Consumer.(survey_data)
